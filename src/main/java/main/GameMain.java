/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Names: Jack Fiala, Jules Ward
 * Date: 5/2/21
 * Time: 11:09 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: GameMain
 *
 * Description: This class acts as the main of our program, it creates our scene and calls our other classes to do
 * our calculation and display our game menu and game!
 * ****************************************
 */

package main;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GameMain extends Application {

    /** Creates an Instance variable of the view */
    private GameView theView;

    /** Creates an Instance variable of the controller */
    private GameController theController;

    /** Creates an Instance variable of the model */
    private GameModel theModel;

    /** Creates a timeline variable to foster the side scrolling */
    private Timeline timeline;

    /**
     * keyframe object is responsible for representing the correct state of the application in the view
     */
    private KeyFrame kf;

    /** Creates a scene variable to create the game scene*/
    private Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Initializes our game and MVC
     * @throws Exception
     */
    @Override
    public void init() throws Exception {

        super.init();
        this.theModel = new GameModel();
        this.theView= new GameView(theModel);
        this.theController = new GameController(theView, theModel);


    }

    /**
     * start - the primary method that is called by JavaFX application to set up the initial stage
     * @param primaryStage
     * @throws InterruptedException
     */
    @Override
    public void start(Stage primaryStage) throws InterruptedException {

        //Add the scene graph to stage
        primaryStage.setWidth(GameModel.GAME_WIDTH);
        primaryStage.setHeight(GameModel.GAME_HEIGHT);
        primaryStage.setResizable(false);

        //Set the title for the main window
        primaryStage.setTitle("House of Flappers");

        //creates the timelime object
        timeline = new Timeline();
        //runs for until we stop it
        timeline.setCycleCount(Animation.INDEFINITE);

        //create the keyframe
        createKeyframe();

        //add keyframe to timeline
        timeline.getKeyFrames().addAll(kf);
        timeline.play();

        scene = new Scene(theView.getRoot());

        //setup jump and starting controls
        scene.setOnKeyPressed(event -> {
            switch(event.getCode()){
                case SPACE:
                case UP:
                case W:
                    if (theModel.getState() == StateEnum.STARTING) {
                        theModel.setState(StateEnum.PLAYING);
                    }
                    theModel.getBird().setJumpValue(Flapper.JUMP_FRAMES);
                    break;
            }
        });

        scene.setOnMouseClicked(event -> {
            if (theModel.getState() == StateEnum.STARTING) {
                theModel.setState(StateEnum.PLAYING);
            }
            theModel.getBird().setJumpValue(Flapper.JUMP_FRAMES);
        });

        primaryStage.setScene(scene);

        //Display the scene
        primaryStage.show();
    }

    /**
     * Create keyframe object, responsible for handling the different states of the application as well as refreshing the model and view if the user is playing
     */
    private void createKeyframe(){

        kf = new KeyFrame(Duration.millis(20), event -> {

            //if in the starting menu
            if (theModel.getState() == StateEnum.STARTING){

                //only run this code if the state just switched to being in the starting state, for more efficient code
                if(theModel.isNewState()) {
                    theModel.restartGame();
                    theView.clearRoot();
                    theView.restartView();
                    theModel.updatePrev();
                }
            }

            //if playing
            if (theModel.getState() == StateEnum.PLAYING) {

                if(theModel.isNewState()) {
                    theView.removeStartLabel();
                    theModel.updatePrev();
                }

                //refresh the game, and check for collisions
                theModel.refreshModel();
                theView.refreshView();
                theModel.checkCollision();
            }

            //if die, in game over menu
            if (theModel.getState() == StateEnum.GAME_OVER) {

                //again, only run if the state just changed to this
                if(theModel.getPrevState() == StateEnum.PLAYING) {
                    theView.clearRoot();
                    theView.gameOverVisuals();
                    theModel.updatePrev();
                }
            }

            //if in main menu
            if(theModel.getState() == StateEnum.MAIN_MENU) {
                if(theModel.isNewState()) {
                    theView.clearRoot();
                    theView.mainMenuVisuals();
                    theModel.updatePrev();
                }
            }

            //if customizing
            if(theModel.getState() == StateEnum.CUSTOMIZING) {
                if(theModel.isNewState()) {
                    theView.clearRoot();
                    theView.customizeVisuals();
                    theModel.updatePrev();
                }
            }

            //if leaderboarding
            if(theModel.getState() == StateEnum.LEADERBOARDING) {
                if(theModel.isNewState()) {
                    theView.clearRoot();
                    theView.leaderboardVisuals();
                    theModel.updatePrev();
                }
            }

            // if exiting the game
            if (theModel.getState() == StateEnum.QUITTING) {

                //stop the timeline before exiting
                timeline.stop();
                Platform.exit();

            }
        });
    }
}
