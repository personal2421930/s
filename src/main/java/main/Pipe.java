/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jack Fiala
 * Date: 5/2/21
 * Time: 10:47 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: Pipe
 *
 * Description: This class creates our rectangles that act as our pipes and refreshes their position.
 * ****************************************
 */
package main;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.Random;

public class Pipe {

    /** The width of our pipes */
    public static final int WIDTH = 125;

    /** The width of the gap between the pairs of pipes */
    public static final int GAP_HEIGHT = 225;

    /** The velocity the pipes move at */
    private static final int VEL = 5;

    /** The middle Y coordinate between the gap */
    private int midHeight;

    /** The color of the pipes */
    private static Paint fillColor = Color.GREEN;

    /** Creates a variable of type rectangle for the lower pipe */
    private Rectangle lower;

    /** Creates a variable of type rectangle for the upper pipe */
    private Rectangle upper;

    /** The xCord of the pipes */
    private int xCord;

    /** The enum state of the pipe */
    private PipeState pipeState;

    /**
     * Constructor that initializes and sets up our pipes at the correct xCords
     * @param xCord X coordinate of pipe
     */
    public Pipe(int xCord) {

        Random rng = new Random();
        this.midHeight = rng.nextInt(GameModel.GAME_HEIGHT -2*(GAP_HEIGHT)) + GAP_HEIGHT;
        this.xCord = xCord;
        this.setupRects();
        this.setPipeState(PipeState.NEW);
    }

    /**
     * This method creates our upper and lower rectangles that acts as our pipes
     */
    private void setupRects() {

        double lowerHeight = GameModel.GAME_HEIGHT- midHeight- GAP_HEIGHT/2;
        double upperHeight = midHeight- GAP_HEIGHT/2;
        this.lower = new Rectangle(xCord,midHeight + GAP_HEIGHT/2,WIDTH, lowerHeight);
        this.lower.setFill(fillColor);
        this.lower.setStroke(Color.BLACK);
        this.lower.setStrokeWidth(5);
        this.upper = new Rectangle(xCord,0,WIDTH, upperHeight);
        this.upper.setFill(fillColor);
        this.upper.setStroke(Color.BLACK);
        this.upper.setStrokeWidth(5);
    }

    /**
     * This refreshes the X coordinate of the upper and lower pipes
     */
    public void refreshPipe(){
        this.xCord -= VEL;
        this.upper.setX(xCord);
        this.lower.setX(xCord);
    }

    public Rectangle getLower() {
        return lower;
    }

    public Rectangle getUpper() {
        return upper;
    }

    public int getxCord() {
        return xCord;
    }

    public void setxCord(int xCord) {
        this.xCord = xCord;
        this.lower.setX(xCord);
        this.upper.setX(xCord);
    }

    public PipeState getPipeState() {
        return pipeState;
    }

    public void setPipeState(PipeState pipeState) {
        this.pipeState = pipeState;
    }
}