/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jack Fiala
 * Date: 5/12/21
 * Time: 2:09 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: StateEnums
 *
 * Description: Organizes the state of the game depending on where the user is within the game, from the game menu to
 * the game itself, to quitting.
 * ****************************************
 */
package main;

/**
 * Allows for different states of where the game is at, from the starting menu, to quitting to simplify code
 */
public enum StateEnum {

    /**
     * The program is on the starting screen
     */
    STARTING,
    /**
     * The program is playing the game
     */
    PLAYING,
    /**
     * The user died, so the program is in the game over menu
     */
    GAME_OVER,
    /**
     * The program is in the main menu
     */
    MAIN_MENU,
    /**
     * The program needs to quit
     */
    QUITTING,
    /**
     * The program is in the customization menu
     */
    CUSTOMIZING,
    /**
     * The program is in the leaderboard menu
     */
    LEADERBOARDING
}
