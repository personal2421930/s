/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Names: Jack Fiala, Jules Ward, Daniela Bellini, Taylor Birch
 * Date: 5/2/21
 * Time: 11:09 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: GameView
 *
 * Description: This class acts as the viewer of our program, getting all our info and creating our actual root,
 * buttons, labels and generally the whole GUI. This allows the user to see our game on their screen!
 * ****************************************
 */
package main;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class  GameView {

    /** A model variable to access the Model*/
    private GameModel theModel;

    /** Creates a root variable */
    private Group root;

    /** A variable of type label to have the score displayed on screen */
    private Label scoreBoard;

    /** VBox for the game over menu layout */
    private VBox gameOverMenu;

    /** A variable of type label to have the end score displayed on screen */
    private Label endScoreLabel;

    /** A variable of type label to tell the user to press space to start the game */
    private Label startLabel;

    /** VBox for the main menu layout */
    private VBox mainMenu;

    /** Play button */
    private Button playButton;

    /** Customize button */
    private Button customizeButton;

    /** Leaderboard button */
    private Button leaderboardButton;

    /** Quit button */
    private Button quitButton;

    /** VBox for the leaderboard screen */
    private VBox leaderboard;

    /** Back button */
    private Button backButton;

    /** VBox for the customize menu */
    private VBox customizeMenu;

    /** Create a HBox to put the pictures side to side */
    private HBox pictureHBox;

    /** Create a HBox to put the checkboxes under the pictures */
    private HBox radioHBox;

    /** Simple text at the top to display customize */
    private Label customizeText;

    /** RadioButton for playing with the Doge character */
    private RadioButton playDoge;

    /** RadioButton for playing with the Elsa character */
    private RadioButton playElsa;

    /** RadioButton for playing with the Dancy character */
    private RadioButton playDancy;

    /** Button to play the game again */
    private Button playAgainButtonOver;

    /** Button to return to the main menu */
    private Button returnToMenuButtonOver;

    /** Button to quit */
    private Button quitButtonOver;

    /** A label for the title */
    private Label title;

    /**
     * This is our View constructor, this creates our root and our list of pipes, this initializes our bird, our pipes
     * and the scoreboard
     *
     * @param theModel This takes in the model to ensure they are connected
     * @throws InterruptedException
     */
    public GameView(GameModel theModel) throws InterruptedException {

        this.theModel = theModel;
        root = new Group();
        createMainMenu();
        createCustomizeMenu();
        createLeaderboard();
        createGameOverMenu();
        restartView();
    }

    /**
     * This method creates our pipes, bird and scoreboard onto our GUI.
     */
    public void restartView() {

        addBird();
        createScoreBoard();
        refreshView();
        createStartLabel();
    }

    /**
     * This adds our character to the GUI.
     */
    private void addBird(){
        root.getChildren().add(theModel.getBird().getSquare());
    }

    /**
     * Method used to add our Pipes onto our root
     * @param pipe The pipe being added to our root
     */
    private void insertPipe(Pipe pipe) {
        root.getChildren().add(pipe.getLower());
        root.getChildren().add(pipe.getUpper());
    }

    /**
     * Method used to remove our Pipes onto our root
     * @param pipe The pipe being remove from our root
     */
    private void removePipe(Pipe pipe) {
        root.getChildren().remove(pipe.getLower());
        root.getChildren().remove(pipe.getUpper());
    }

    /**
     * This method refreshes our bird and our pipes, ensuring the users GUI is up to date.
     *
     * @return the root
     */
    public void refreshView() {

        for (Pipe pipe: theModel.getPipeList() ) {

            if (pipe.getPipeState() == PipeState.NEW) {
                insertPipe(pipe);
                pipe.setPipeState(PipeState.LIVE);
            }
            else if (pipe.getPipeState() == PipeState.DELETE) {
                removePipe(pipe);
            }
        }

        scoreBoard.setText("" + theModel.getScore());
        scoreBoard.toFront();
    }

    /**
     * Creates the start label to our game "Press Space to Start!"
     */
    private void createStartLabel() {

        startLabel = new Label("Press Space to Start!");
        startLabel.setPrefSize(800,75);
        Border labelBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(4), BorderWidths.DEFAULT));
        startLabel.setBorder(labelBorder);
        startLabel.setAlignment(Pos.CENTER);
        root.getChildren().add(startLabel);
        startLabel.setFont(new Font("Arial",35));
        startLabel.setBackground(new Background(new BackgroundFill(Color.rgb(255, 255, 0, 1), new CornerRadii(4.0), new Insets(-0))));
        startLabel.relocate(GameModel.GAME_WIDTH/2 -800/2, 200);
        startLabel.toFront();
    }

    /**
     * Removes the start label to our game once the game starts.
     */
    public void removeStartLabel() {
        root.getChildren().remove(startLabel);
    }

    /**
     * This method initialized and creates the scoreboard onto our GUI.
     */
    public void createScoreBoard() {

        scoreBoard = new Label("Score: 0");
        scoreBoard.setPrefSize(100,75);

        Border labelBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(4), BorderWidths.DEFAULT));
        scoreBoard.setBorder(labelBorder);
        scoreBoard.setAlignment(Pos.CENTER);
        root.getChildren().add(scoreBoard);

        scoreBoard.setFont(new Font("Arial",24));
        scoreBoard.setBackground(new Background(new BackgroundFill(Color.rgb(255, 255, 0, 1), new CornerRadii(4.0), new Insets(-0))));
        scoreBoard.relocate(GameModel.GAME_WIDTH/2 -100/2, 100);
        scoreBoard.toFront();
    }

    /**
     * Sets up the game over visuals scene once the player loses.
     */
    public void gameOverVisuals(){

        endScoreLabel.setText("Score: " + theModel.getScore());
        root.getChildren().addAll(gameOverMenu);
        gameOverMenu.setAlignment(Pos.CENTER);
    }

    /**
     * Create the game over menu and visuals.
     */
    private void createGameOverMenu() {

        setUpGameOverMenu();
        createPlayAgainButton();
        createReturnToMenuButton();
        createGameOverQuitButton();
        createEndScoreLabel();

        gameOverMenu.getChildren().addAll(endScoreLabel,playAgainButtonOver, returnToMenuButtonOver, quitButtonOver);
        playAgainButtonOver.setAlignment(Pos.CENTER);
        returnToMenuButtonOver.setAlignment(Pos.CENTER);
        returnToMenuButtonOver.setAlignment(Pos.CENTER);
        quitButtonOver.setAlignment(Pos.CENTER);
    }

    /**
     * Create the quit button in the game over menu.
     */
    private void createGameOverQuitButton() {

        quitButtonOver = new Button("Quit");
        quitButtonOver.setStyle("-fx-background-color: #63e5ff");
        quitButtonOver.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        quitButtonOver.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Create the return to main menu button.
     */
    private void createReturnToMenuButton() {

        returnToMenuButtonOver = new Button("Return to Main Menu");
        returnToMenuButtonOver.setStyle("-fx-background-color: #b1f2ff");
        returnToMenuButtonOver.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        returnToMenuButtonOver.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Create the play again button.
     */
    private void createPlayAgainButton() {

        playAgainButtonOver = new Button("Play Again");
        playAgainButtonOver.setStyle("-fx-background-color: #d8f9ff");
        playAgainButtonOver.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        playAgainButtonOver.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Creates the end score label that displays the final score on the game over screen.
     */
    private void createEndScoreLabel() {

        endScoreLabel = new Label("Score: " + theModel.getScore());
        endScoreLabel.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Visually set up the game over menu.
     */
    private void setUpGameOverMenu() {

        gameOverMenu = new VBox(16);
        gameOverMenu.setPrefWidth(GameModel.GAME_WIDTH);
        gameOverMenu.setPrefHeight(GameModel.GAME_HEIGHT);
        gameOverMenu.setPadding(new Insets(15));
    }

    /**
     * Visually set up the main menu by adding it to the root.
     */
    public void mainMenuVisuals() {
        root.getChildren().add(mainMenu);
    }

    /**
     * Create the entire main menu screen.
     */
    private void createMainMenu() {

        setUpMainMenu();

        // Play button
        createPlayButton();

        // Customize button
        createCustomizeButton();

        // Leaderboard button
        createLeaderboardButton();

        // Quit button
        createQuitButton();

        createTitleLabel();

        mainMenu.getChildren().addAll(title, playButton, customizeButton, quitButton);

        // Make the visual pretty by aligning and padding the buttons
        mainMenu.setAlignment(Pos.CENTER);
        mainMenu.setBackground(new Background(new BackgroundFill(Color.SPRINGGREEN, CornerRadii.EMPTY,
                Insets.EMPTY)));
    }

    /**
     * Visually set up the main menu screen.
     */
    private void setUpMainMenu() {

        mainMenu = new VBox(16);
        mainMenu.setPrefWidth(GameModel.GAME_WIDTH);
        mainMenu.setPrefHeight(GameModel.GAME_HEIGHT);
        mainMenu.setPadding(new Insets(15));
    }

    /**
     * Create the quit button.
     */
    private void createQuitButton() {

        quitButton = new Button("Quit");
        quitButton.setStyle("-fx-background-color: #63e5ff");
        quitButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        quitButton.setFont(Font.font("Monospaced", 27));
    }

    private void createTitleLabel() {

        title = new Label("House of Flappers");
        title.setFont(Font.font("Monospaced", 35));
        title.setPrefSize(500,75);
        Border labelBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(4), BorderWidths.DEFAULT));
        title.setBorder(labelBorder);
        title.setAlignment(Pos.CENTER);
        title.setBackground(new Background(new BackgroundFill(Color.rgb(200, 0, 200, 1), new CornerRadii(4.0), new Insets(-0))));
    }

    /**
     * Create the leaderboard button.
     */
    private void createLeaderboardButton() {

        leaderboardButton = new Button("Leaderboard");
        leaderboardButton.setStyle("-fx-background-color: #8aecff");
        leaderboardButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        leaderboardButton.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Create the customize button.
     */
    private void createCustomizeButton() {

        customizeButton = new Button("Customize");
        customizeButton.setStyle("-fx-background-color: #b1f2ff");
        customizeButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        customizeButton.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Create the play button.
     */
    private void createPlayButton() {

        // Create the play button and customize
        playButton = new Button("Play");
        playButton.setStyle("-fx-background-color: #d8f9ff");
        playButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        playButton.setFont(Font.font("Monospaced", 27));
    }

    /**
     * Add the leaderboard visuals to the root.
     */
    public void leaderboardVisuals() {
        root.getChildren().add(leaderboard);
    }

    /**
     * Create the leaderboard.
     */
    public void createLeaderboard() {

        leaderboard = new VBox();
        leaderboard.setPrefWidth(GameModel.GAME_WIDTH);
        leaderboard.setPrefHeight(GameModel.GAME_HEIGHT);
        leaderboard.setPadding(new Insets(15));

        // Set up the back button
        createBackButton();

        leaderboard.setBackground(new Background(new BackgroundFill(Color.LAVENDER, CornerRadii.EMPTY,
                Insets.EMPTY)));

        leaderboard.getChildren().add(backButton);
        leaderboard.setAlignment(Pos.BOTTOM_CENTER);
    }

    /**
     * Create the back button.
     */
    private void createBackButton() {

        // Create the back button
        backButton = new Button("Back");
        backButton.setStyle("-fx-background-color: #73c2fb");
        backButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.DASHED,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        backButton.setFont(Font.font("Monospaced", 20));
        backButton.setOnAction(event -> {
            theModel.setState(StateEnum.MAIN_MENU);
        });
    }

    /**
     * Add the customize visuals to the root so that it will show up on the screen.
     */
    public void customizeVisuals() {

        root.getChildren().add(customizeMenu);
        customizeMenu.setAlignment(Pos.CENTER);
    }

    /**
     * Visually create the customize menu.
     */
    private void createCustomizeMenu(){

        setUpCustomizeMenu();
        createChooseCharacterText();
        setUpCharacterVisuals();
        setUpRadioButtons();
        createBackButton();

        customizeMenu.getChildren().add(backButton);
        backButton.setAlignment(Pos.CENTER);
    }

    /**
     * Set up the radio buttons that decide which character to play with.
     */
    private void setUpRadioButtons() {

        radioHBox = new HBox();
        radioHBox.setPadding(new Insets(15, 12, 15, 40));
        radioHBox.setSpacing(60);
        createRadioBtns();
        customizeMenu.getChildren().add(radioHBox);
        radioHBox.setAlignment(Pos.CENTER);
    }

    /**
     * Set up the character displays so that the user can see the different character options.
     */
    private void setUpCharacterVisuals() {

        pictureHBox = new HBox();
        pictureHBox.setPadding(new Insets(15, 12, 15, 12));
        pictureHBox.setSpacing(30);
        createPFP("src/main/resources/dogeFINAL.png");
        createPFP("src/main/resources/elsa.png");
        createPFP("src/main/resources/dancy.png");

        customizeMenu.getChildren().add(pictureHBox);
        pictureHBox.setAlignment(Pos.CENTER);
    }

    /**
     * Set the scene for the customize menu.
     */
    private void setUpCustomizeMenu() {

        customizeMenu = new VBox();
        customizeMenu.setPrefWidth(GameModel.GAME_WIDTH);
        customizeMenu.setPrefHeight(GameModel.GAME_HEIGHT);
        customizeMenu.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY,
                Insets.EMPTY)));
        customizeMenu.setPadding(new Insets(15));
    }

    /**
     * Find and display the character image.
     * @param pathName - where to find the picture
     */
    private void createPFP(String pathName){

        // Show the two character options
        ImageView view = new ImageView();
        Image flappyBird = null;
        try {
            flappyBird = new Image(new FileInputStream(pathName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        view.setImage(flappyBird);
        view.setFitHeight(80);
        view.setPreserveRatio(true);
        pictureHBox.getChildren().add(view);
    }

    /**
     * Create the radio buttons that will be used by the user to pick a character.
     */
    private void createRadioBtns() {

        playDoge = new RadioButton("Doge");
        playElsa = new RadioButton("Elsa");
        playDancy = new RadioButton("Dancy");

        ToggleGroup playersGroup = new ToggleGroup();
        playDoge.setToggleGroup(playersGroup);
        playElsa.setToggleGroup(playersGroup);
        playDancy.setToggleGroup(playersGroup);
        playDoge.setSelected(true);

        //add buttons to left pane
        radioHBox.getChildren().addAll(playDoge, playElsa, playDancy);
    }

    /**
     * Text that tells the user to choose a character.
     */
    private void createChooseCharacterText() {

        // Create the choose a character at the top of the screen
        customizeText = new Label("Choose a character!");
        customizeText.setFont(Font.font("Monospaced", FontWeight.BOLD, 20));
        customizeMenu.getChildren().add(customizeText);
        customizeText.setAlignment(Pos.CENTER);
    }

    public void clearRoot() {
        root.getChildren().clear();
    }

    public Group getRoot() {
        return root;
    }

    public Button getBackButton() {
        return backButton;
    }

    public Button getCustomizeButton() {
        return customizeButton;
    }

    public VBox getCustomizeMenu() {
        return customizeMenu;
    }

    public Button getLeaderboardButton() {
        return leaderboardButton;
    }

    public Button getPlayButton() {
        return playButton;
    }

    public Button getQuitButton() {
        return quitButton;
    }

    public RadioButton getPlayDancy() {
        return playDancy;
    }

    public RadioButton getPlayDoge() {
        return playDoge;
    }

    public RadioButton getPlayElsa() {
        return playElsa;
    }

    public Button getPlayAgainButtonOver() {
        return playAgainButtonOver;
    }

    public Button getQuitButtonOver() {
        return quitButtonOver;
    }

    public Button getReturnToMenuButtonOver() {
        return returnToMenuButtonOver;
    }
}