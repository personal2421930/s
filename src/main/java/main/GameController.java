/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jack Fiala, Jules Ward
 * Date: 5/2/21
 * Time: 11:09 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: GameController
 *
 * Description: This class acts as the controller of our program. It uses event handlers to set our different game state
 * enums and handlers to change the character selected.
 * ****************************************
 */
package main;

public class GameController {

    /** Creates an Instance variable of the model*/
    private GameModel theModel;

    private GameMain gameMain;

    /** Creates an Instance variable of the view*/
    private GameView theView;

    /**
     * Use event handlers to set the state to change the current game state, and change the pathname of the character
     * image
     *
     * @param theView An instance of the View Class
     * @param theModel An instance of the Model class
     */
    public GameController(GameView theView, GameModel theModel){

        this.theView = theView;
        this.theModel = theModel;

        theView.getPlayButton().setOnAction(event -> {
            theModel.setState(StateEnum.STARTING);
        });

        theView.getCustomizeButton().setOnAction(event -> {
            theModel.setState(StateEnum.CUSTOMIZING);
        });


        //theView.getLeaderboardButton().setOnAction( event -> {
            //theModel.setState(StateEnum.LEADERBOARDING);
        //});

        theView.getQuitButton().setOnAction(event -> {
            theModel.setState(StateEnum.QUITTING);
        });

        theView.getPlayDoge().setOnAction(event -> {
            theModel.setPathName("src/main/resources/dogeFINAL.png");
        });
        theView.getPlayElsa().setOnAction(event -> {
            theModel.setPathName("src/main/resources/elsa.png");
        });
        theView.getPlayDancy().setOnAction(event -> {
            theModel.setPathName("src/main/resources/dancy.png");
        });

        theView.getPlayAgainButtonOver().setOnAction(event -> {
            theModel.setState(StateEnum.STARTING);
        });
        theView.getQuitButtonOver().setOnAction(event -> {
            theModel.setState(StateEnum.QUITTING);
        });
        theView.getReturnToMenuButtonOver().setOnAction(event -> {
            theModel.setState(StateEnum.MAIN_MENU);
        });
    }
}