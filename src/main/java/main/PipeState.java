/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jack Fiala
 * Date: 5/16/21
 * Time: 9:52 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: PipeState
 *
 * Description: Organizes the state of the pipes depending on if they should be new one's created, if they are on screen
 * or if they need to be deleted. *
 * ****************************************
 */
package main;

/**
 * Allows for different state within the pipes to keep track of if the we need new pipes, if they are on screen or if
 * they need to be deleted.
 */
public enum PipeState {

    /**
     * Pipe is in the arraylist, but not yet in the view
     */
    NEW,
    /**
     * Pipe is in the arraylist and in the view
     */
    LIVE,

    /**
     * Pipe needs to be removed from arraylist and the view
     */
    DELETE
}