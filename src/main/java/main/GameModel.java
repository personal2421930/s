/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jack Fiala
 * Date: 5/2/21
 * Time: 11:09 PM
 *
 * Project: csci205SP21FinalProject * Package: main * Class: GameModel
 *
 * Description: This class acts as the model of our program, it does the calculations for our game, checks for the
 * collision between the character and pipes, refreshes our where our pipes and character is, adds/removes
 * pipes from the screen and restarts our game when necessary.
 * ****************************************
 */
package main;

import java.util.ArrayList;
import java.util.Iterator;

public class GameModel {

    /** Height of game window */
    public static final int GAME_HEIGHT = 700;

    /** Width of game window */
    public static final int GAME_WIDTH = 1500;

    /** The amount of pipes displayed on screen */
    static final int PIPES_ON_SCREEN = 4;

    /** The width of the pipes */
    static final int PIPE_SPACING = GAME_WIDTH /PIPES_ON_SCREEN;

    /** Integer variable to set the current state to STARTING */
    private StateEnum state = StateEnum.MAIN_MENU;

    /** Integer variable to set the previous state to STARTING */
    public StateEnum prevState = StateEnum.GAME_OVER;

    /** A variable to keep track of the score */
    private int score;

    /** A variable for our list of pipes */
    private ArrayList<Pipe> pipeList;

    /** A variable to iterate through the pipes */
    private Iterator<Pipe> pipeItr;

    /** Creates a new bird variable */
    private Flapper bird;

    /** String variable to access the path of our character images*/
    private String pathName;

    /**
     * Constructor for model and reinitialize game
     */
    public GameModel() {
        restartGame();
    }

    /**
     * Overridden constructor for model and reinitialize game during testing
     * @param test make sure bird is created with test path name
     */
    public GameModel(String test) {

        if(test.compareTo("TEST") == 0) {
            pathName = "TEST";
        }
        restartGame();
    }

    /**
     * Reinitialize our game, with the score, pipes and character selected.
     */
    public void restartGame() {
        score = 0;
        pipeList = new ArrayList<>();
        if (pathName == null) {
            bird = new Flapper();
        }
        else {
            bird = new Flapper(pathName);
        }

        //add initial Pipes
        for (int i = 0; i < GameModel.PIPES_ON_SCREEN; i++) {
            addPipe(GameModel.GAME_WIDTH - i* GameModel.PIPE_SPACING);
        }
    }

    /**
     * A simple method that checks if the bird has collided with the pipe and if so changes the variable isAlive to
     * false
     */
    public void checkCollision() {

        for (Pipe pipe: pipeList) {
            if (pipe.getLower().getBoundsInParent().intersects(bird.getSquare().getBoundsInParent())) {
                setState(StateEnum.GAME_OVER);
            } else if (pipe.getUpper().getBoundsInParent().intersects(bird.getSquare().getBoundsInParent())) {
                setState(StateEnum.GAME_OVER);
            }
        }

        if ((bird.getyCord() <= 0 ) || (bird.getyCord() >= (GameModel.GAME_HEIGHT-1.5*Flapper.SIZE )))  {
            setState(StateEnum.GAME_OVER);
        }
    }

    /**
     * Refreshes our screen, meaning the pipes are either added or removed, and the character is also refreshed.
     * Used to keep the game screen up to date.
     */
    public void refreshModel() {

        Pipe currentPipe;
        pipeItr = pipeList.iterator();
        boolean addingPipe = false;
        bird.refreshBird();

        while (pipeItr.hasNext()) {

            currentPipe = pipeItr.next();
            currentPipe.refreshPipe();
            if (currentPipe.getPipeState() == PipeState.DELETE) {
                pipeItr.remove();
            }
            else if (currentPipe.getxCord()<= 0) {
                addingPipe = true;
                currentPipe.setPipeState(PipeState.DELETE);
                incScore();
            }
        }
        if (addingPipe) {
            addPipe();
        }
    }

    /**
     * This will create and add a new pipe to our list and onto the GUI.
     */
    private void addPipe(){

        Pipe newPipe = new Pipe(GameModel.GAME_WIDTH);
        pipeList.add(newPipe);
    }
    /**
     * This will create and add a new pipe, at the appropriate x coordinate, to our list and onto the GUI.
     * @param xCord The x coordinate at which the pipe should be outputted to.
     */
    private void addPipe(int xCord) {

        Pipe newPipe = new Pipe(xCord);
        pipeList.add(newPipe);
    }

    public int getScore() {
        return score;
    }

    public void incScore() {
        score ++;
    }

    public StateEnum getState() {
        return this.state;
    }

    public void setState(StateEnum newState) {
        this.state = newState;
    }

    public boolean isNewState() { return !(this.state == this.prevState);}

    public StateEnum getPrevState() {
        return this.prevState;
    }

    public void updatePrev() {
        this.prevState = this.state;
    }

    public Flapper getBird(){return bird;}

    public ArrayList<Pipe> getPipeList() {
        return pipeList;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }
}

