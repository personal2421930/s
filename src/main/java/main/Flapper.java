/* *****************************************
 * CSCI205 - Software Engineering and Design
 * Spring 2021
 * Instructor: Prof. Chris Dancy
 *
 * Name: Jules Ward, Kiz Ononuju, Jack Fiala
 * Date: 5/4/2021
 * Time: 4:00 PM
 *
 * Project: csci205SP21FinalProject
 * Package: main
 * Class: Flapper
 *
 * Description: This is the class that creates our character hitbox/square. It initializes our character and refreshes
 * the characters position whenever the user presses the jump button!
 *
 * ****************************************
 */
package main;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.io.File;

public class Flapper {

    /**
     * This variable determines the size of our character
     */
    public static final int SIZE = 50;

    /**
     * The 'velocity' at which the character falls
     */
    private static final int VELDOWN = 7;

    /**
     * The 'velocity' at which the character flies
     */
    private static final int VELUP = 10;

    /**
     * This value is used to calculate the amount of time that the bird should be jumping
     */
    public static final int JUMP_FRAMES = 10;

    /**
     * Color of our square
     */
    private static Paint fillColor = Color.BLUE;

    /**
     * Creates our square
     */
    private Rectangle square;

    /**
     * The y coordinate where our character starts at
     */
    private int yCord = 350;

    /**
     * The x coordinate where our character starts at and will remain
     */
    private int xCord = 100;

    /** A variable to keep track of when not to jump */
    private int jumpValue;

    /** Acts as the fileName for our character image to allow us to use different images for our character */
    private String fileName;

    /**
     * Default constructor to sets up our character and uses original character image
     */
    public Flapper() {

        this.fileName = "src/main/resources/dogeFINAL.png";
        this.setupSquare();
    }

    /**
     * Overridden constructor to the flapper class that creates our square, and allows for users to change character
     * based on the path
     * There is also TEST functionality
     * @param path this is the path to our character image file, this allows us to set a different character image
     */
    public Flapper(String path) {

        if(path.compareTo("TEST") ==0) {
            this.fileName = "src/main/resources/dogeFINAL.png";
            this.setupSquareTest();
        }
        else {
            this.fileName = path;
            this.setupSquare();
        }
    }

    /**
     * Creates a square of size SIZE used to check collisions with pipes and sets it to a specified color
     */
    private void setupSquare() {
        this.square = new Rectangle(xCord, yCord, SIZE, SIZE);
        File file = new File(this.fileName);
        Image character = new Image(file.toURI().toString(), 50, 50, true, true);
        this.square.setFill(new ImagePattern(character));
    }

    /**
     * Test set up function of square, does everything except fill image because causes errors
     */
    private void setupSquareTest() {
        this.square = new Rectangle(xCord, yCord, SIZE, SIZE);
        File file = new File(this.fileName);
//        Image character = new Image(file.toURI().toString(), 50, 50, true, true);
//        this.square.setFill(new ImagePattern(character));
    }


    /**
     * Moves the bird downwards if not currently jumping, and upwards otherwise
     */
    public void refreshBird() {
        if (jumpValue != 0) {
            this.yCord -= VELUP;
            this.square.setY(yCord);
            this.jumpValue--;

        } else {
            this.yCord += VELDOWN;
            this.square.setY(yCord);
        }
    }

    public Rectangle getSquare() {
        return square;
    }

    public void setJumpValue(int jumpValue) {
        this.jumpValue = jumpValue;
    }

    public int getyCord() {
        return yCord;
    }

    public void setyCord(int yCord) {
        this.yCord = yCord;
        this.square.setY(yCord);
    }

    public int getxCord() {
        return xCord;
    }

    public String getFileName() {
        return fileName;
    }
}
