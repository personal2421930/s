package main;

import javafx.scene.shape.Rectangle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test the pipe class, including the geometry and refreshing
 */
class PipeTest {

    Pipe pipe;

    @BeforeEach
    void setUp() {
        pipe = new Pipe(100);
    }

    /**
     * Test the x coordinate and make sure the pipe moves well
     */
    @Test
    void testXCoord() {

        assertEquals(pipe.getxCord(), 100);
        pipe.refreshPipe();
        assertEquals(pipe.getxCord(), 95);
        pipe.refreshPipe();
        assertEquals(pipe.getxCord(), 90);
        pipe.refreshPipe();
        assertEquals(pipe.getxCord(), 85);
    }

    /**
     * Test both rectangles of the pipe, upper and lower
     */
    @Test
    void testRects() {

        Rectangle upper = pipe.getUpper();
        Rectangle lower = pipe.getLower();

        assertEquals(upper.getX(), 100);
        assertEquals(lower.getX(), 100);
        assertEquals(upper.getWidth(), Pipe.WIDTH);
        assertEquals(lower.getWidth(), Pipe.WIDTH);
        pipe.refreshPipe();
        assertEquals(upper.getX(), 95);
        assertEquals(lower.getX(), 95);
        assertEquals(lower.getHeight()+upper.getHeight(), GameModel.GAME_HEIGHT-Pipe.GAP_HEIGHT+1);
    }
}