package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test various aspects of the model
 */
class GameModelTest {

    GameModel theModel;
    @BeforeEach
    void setUp() {
        theModel = new GameModel("TEST");
    }

    /**
     * Test that the scoring works
     */
    @Test
    void testScore() {
        assertEquals(0,theModel.getScore());
        theModel.incScore();
        assertEquals(1,theModel.getScore());
        theModel.incScore();
        assertEquals(2,theModel.getScore());
        theModel.incScore();
        assertEquals(3,theModel.getScore());
    }

    /**
     * Test the array list of pipes
     */
    @Test
    void testPipeList() {

        ArrayList<Pipe> pipeList = theModel.getPipeList();
        assertEquals(theModel.PIPES_ON_SCREEN,pipeList.size());
        for (Pipe p: pipeList) {
            assertEquals(true,p.getxCord() < theModel.GAME_WIDTH+1);
            assertEquals(true,p.getxCord()> 0);

        }
    }

    /**
     * Test that the model refreshes everything properly
     */
    @Test
    void testRefresh() {
        theModel.refreshModel();
        ArrayList<Pipe> pipeList = theModel.getPipeList();
        assertEquals(theModel.PIPES_ON_SCREEN, pipeList.size());
        for (Pipe p : pipeList) {
            assertEquals(true, p.getxCord() < theModel.GAME_WIDTH);
            assertEquals(true, p.getxCord() > 0);

        }

        assertEquals(theModel.getBird().getyCord(), 357);
    }

    /**
     * Test that the collision detects works properly
     */
    @Test
    void testCollision() {
        theModel.setState(StateEnum.PLAYING);
        while(theModel.getState() == StateEnum.PLAYING) {
            theModel.refreshModel();
            theModel.checkCollision();
        }
        assertEquals(StateEnum.GAME_OVER, theModel.getState());

        theModel.restartGame();
        theModel.setState(StateEnum.PLAYING);
        theModel.getBird().setyCord(-1);
        theModel.checkCollision();
        assertEquals(StateEnum.GAME_OVER, theModel.getState());

        theModel.restartGame();
        theModel.setState(StateEnum.PLAYING);
        theModel.getBird().setyCord(theModel.GAME_HEIGHT+1);
        theModel.checkCollision();
        assertEquals(StateEnum.GAME_OVER, theModel.getState());


    }
}