package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test the flapper class itself, including the positioning and geometry
 */
class FlapperTest {

    Flapper bird;
    @BeforeEach
    void setUp() {
        bird = new Flapper("TEST");
    }

    /**
     * Test that the default filename holds up
     */
    @Test
    void defaultImage() {

        assertEquals("src/main/java/main/dogeFINAL.png",bird.getFileName());

    }

    /**
     * Test the x coordinate of the bird
     */
    @Test
    void testXCoord() {
        assertEquals(100,bird.getxCord());
    }

    /**
     * Test the y cord of the bird after a couple different refreshes
     */
    @Test
    void testYCoord() {
        assertEquals(350,bird.getyCord());
        bird.refreshBird();
        assertEquals(357,bird.getyCord());
        bird.refreshBird();
        assertEquals(364,bird.getyCord());
        bird.setJumpValue(Flapper.JUMP_FRAMES);
        bird.refreshBird();
        assertEquals(354,bird.getyCord());

    }

    /**
     * Test the rectangle shape of the bird, intially and after a refresh
     */
    @Test
    void testRect() {
        assertEquals(350,bird.getSquare().getY());
        assertEquals(100,bird.getSquare().getX());
        assertEquals(50,bird.getSquare().getHeight());
        assertEquals(50,bird.getSquare().getWidth());
        bird.refreshBird();
        assertEquals(357,bird.getSquare().getY());
        assertEquals(100,bird.getSquare().getX());

    }
}