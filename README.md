# CSCI 205 - Software Engineering and Design
Bucknell University
Lewisburg, PA
## Course Info
Instructor: Professor Dancy

Semester: SP21
## Team Information
**The Runtime Terrors!!!!**

Daniela Bellini, Taylor Birch, Jack Fiala, Kizito Ononuju, Jules Ward
## Project Information - House of Flappers
*This was our Final Project for CSCI205!*

Using the knowledge we have obtained over the course of the semester, we were tasked as a group to design an application with a graphical component. We choose to make a platformer-style game, called House of Flappers, similar to the popular game "Flappy Bird". Our goal in creating this game was to make a lightweight, clean interface for the user to interact with that provided a simple yet enjoyable experience.  

The gameplay is relatively straightforward - upon running the game the user is brought to a main menu, where they can choose to select a character in the customization menu, play the game, or quit. Upon choosing to play, they are brought to a starting screen, where once they press a key, the game begins. The goal for the user is to navigate their character through the moving pipes by jumping at the right moments. If the user hits a pipe or goes off the edge of the screen, the user dies and the game ends. The user is then brought to a game over menu, where they can choose to go back to the main menu, play again, or quit.

We didn't accomplish some of the goals we wanted to complete for this game, such as additional customization options and a leaderboard, but still finished with a solid program. Along the way, we learned valuable lessons in Agile Development and the Scrum process, refactoring our code alot and focusing on non-coding elements like certain artifacts and the burndown chart. Overall, we are very happy with how are project turned out and have become better coders/scrummers from it!

## Package Structure

The project directory is organized as follows:
- The "design/" directory contains non-coding OOP design elements, including UML use case, class, and state diagrams, along with our CRC cards. Some artifacts have multiple versions to reflect the initial design versus final implementation
- The "scrum/" directory contains all the scrum elements in their final, most recent version
- The "docs/" directory contains several documents and a presentation, describing different aspects of our project
- The "src/main/java/main/" directory contains all of our application's code for operation
- The "src/test/java/main/" directory contains all of our application's test code, mostly focused on the logic and states of the game. Testing of the visual elements was beyond the scope of this course.
- The "src/main/resources/" directory contains the image files used for the characters in the game, as well as the background music we tried to implement

## Additional Libraries
[JavaFX 14.0.2](https://mvnrepository.com/artifact/org.openjfx/javafx/14.0.2)
## How to run it
In order to run the program, the correct version of javaFX library must be listed in the build.gradle file (should already be there).

The configuration is setup properly in the build.gradle file so that to run the application, just type "gradle run" while in the project directory.

## References
- https://stackoverflow.com/questions/37104215/error-exception-in-thread-javafx-application-thread
- https://www.tutorialspoint.com/java/java_using_iterator.htm
- https://github.com/MompiDevi/Flappy-Bird-in-JavaFx/blob/master/trial.java
- https://zetcode.com/gui/javafx/animation/
- https://stackoverflow.com/questions/24378557/how-to-add-background-colour-to-group-layout-in-javafx

